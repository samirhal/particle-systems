/*
This code is licensed under the Mozilla Public License Version 2.0
(http://opensource.org/licenses/MPL-2.0) � 2014 by Sascha Willems -
http://www.saschawillems.de

This compute shader implements a very basic attraction based particle system
that changes velocities to move the particles towards the target position
*/

#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <iostream>
#include <optional>

#include "ParticleSystem.h"
#include "shader.h"

class glRenderer {
  private:
  GLuint computeshader;
  // GLuint baseshader;
  std::optional<Shader> mainShader;
  GLuint SSBOPos;
  GLuint particleTex;
  float frameDelta = 0.0f;
  bool borderEnabled = true;
  bool pause = false;
  // GLuint loadComputeShader(const char* computeShaderFile);
  // GLuint loadShader(const char* vertexShaderFile,
  //                   const char* fragmentShaderFile);
  void printProgramLog(GLuint shader);
  void printShaderLog(GLuint program);
  void renderColors(float);
  void runComputeShader();
  void update();

  public:
  GLFWwindow* window;
  // int particleCount = 1024;
  size_t allocatedSize = 0;
  ParticleSystem particleSystem;
  glRenderer();
  ~glRenderer();
  void generateShaders();
  void generateBuffers();
  void resetBuffers();
  void generateTextures();
  void renderScene();
  void keyCallback(int key, int scancode, int action, int mods);
};

