#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <algorithm>
#include <glm/glm.hpp>
#include <iostream>
#include <vector>

inline float random(float fMin, float fMax) {
  float fRandNum = (float)rand() / RAND_MAX;
  return fMin + (fMax - fMin) * fRandNum;
}

template <typename I, typename P>
auto stable_partition_position(I f, I l, P p) -> I {
  auto n = l - f;
  if (n == 0) return f;
  if (n == 1) return f + p(f);
  auto m = f + (n / 2);
  return std::rotate(stable_partition_position(f, m, p), m,
                     stable_partition_position(m, l, p));
}

struct ParticleSystem {
  std::vector<float> time_to_live;
  std::vector<glm::vec3> velocities;
  std::vector<glm::vec3> particles;

  bool colorFade = false;
  float color[3] = {255, 255, 255};
  float colVec[3];
  float colorChangeTimer;
  float colorChangeLength;

  ParticleSystem() {
    init(200);
    // color[0] = random(64, 255);
    // color[1] = random(64, 255);
    // color[2] = random(64, 255);
    colorChangeTimer = 1000.0f;
  }

  void reset() {
    for (auto& verticesPos : particles) {
      verticesPos.x = 2 * (float)rand() / RAND_MAX - 1.0;
      verticesPos.y = 1.0f;
      verticesPos.z = 0.0f;
    }

    for (auto& vel : velocities) {
      vel = glm::vec3(0.0, 0.0, 0.0);
    }
  }

  void add_particles(size_t n) {
    increase(n);

    for (auto iter = particles.end() - n; iter < particles.end(); ++iter) {
      iter->x = 2 * (float)rand() / RAND_MAX - 1.0;
      iter->y = 1.0f;
      iter->z = 0.0f;
    }
    for (auto iter = time_to_live.end() - n; iter < time_to_live.end();
         ++iter) {
      *iter = 50.0 + (rand() % 10);
    }
  }

  void decrease(int num) {
    // TODO this is shooting yourself in the foot
    particles.erase(particles.end() - num, particles.end());
    velocities.erase(velocities.end() - num, velocities.end());
    time_to_live.erase(time_to_live.end() - num, time_to_live.end());
  }

  void increase(int num) {
    particles.resize(particles.size() + num);
    velocities.resize(velocities.size() + num, glm::vec3(0.0, 0.0, 0.0));
    time_to_live.resize(time_to_live.size() + num, 50.0);
  }

  void renderColors(float deltaT) {
    if (colorFade) {
      auto frameDelta = deltaT / 0.15;
      colorChangeTimer -= frameDelta * 25.0f;

      if ((colorChangeTimer <= 0.0f) & (colorChangeLength <= 0.0f)) {
        if (random(0.0f, 100.0f) < 50.0f) {
          colVec[0] = (int)random(0.0f, 8.0f) * 32.0f - color[0];
          colVec[1] = (int)random(0.0f, 8.0f) * 32.0f - color[1];
          colVec[2] = (int)random(0.0f, 8.0f) * 32.0f - color[2];

          float vlength = sqrt(colVec[0] * colVec[0] + colVec[1] * colVec[1] +
                               colVec[2] * colVec[2]);
          colorChangeLength = vlength * 2.0f;

          colVec[0] = colVec[0] / vlength;
          colVec[1] = colVec[2] / vlength;
          colVec[2] = colVec[2] / vlength;
        };
        colorChangeTimer = 1000.0f;
      }

      if (colorChangeLength > 0.0f) {
        color[0] += colVec[0] * frameDelta;
        color[1] += colVec[1] * frameDelta;
        color[2] += colVec[2] * frameDelta;

        colorChangeLength -= frameDelta;
      }

    }
    // else {
      // color[0] = 255.0f;
      // color[1] = 64.0f;
      // color[2] = 0.0f;
    // }
  }

  void update(GLFWwindow* window, float deltaT) {
    add_particles(10);
    renderColors(deltaT);
    // int windowWidth, windowHeight;
    // glfwGetWindowSize(window, &windowWidth, &windowHeight);

    // do update
    update_velocities(deltaT);
    update_position(deltaT);
    update_time(deltaT);
    remove_old_particles();
  }

  void update_position(float deltaT) {
    for (auto index = 0u; index < particles.size(); index++) {
      glm::vec3 vPos = {particles[index].x, particles[index].y,
                        particles[index].z};
      vPos += velocities[index] * deltaT;
      particles[index] = vPos;
    }
  }

  // void compute_colisions() {
  // if (vPos.x < -1.0) {
  //   vPos.x = -1.0;
  //   vVel.x = -vVel.x;
  // }
  // if (vPos.x > 1.0) {
  //   vPos.x = 1.0;
  //   vVel.x = -vVel.x;
  // }
  // if (vPos.y < -1.0) {
  //   vPos.y = -1.0;
  //   vVel.y = -vVel.y;
  // }
  // if (vPos.y > 1.0) {
  //   vPos.y = 1.0;
  //   vVel.y = -vVel.y;
  // }
  // }
  void update_velocities(float deltaT) {
    for (uint32_t index = 0; index < particles.size(); index++) {
      glm::vec3 vVel = velocities[index];

      // Calculate new velocity depending on attraction point
      auto t = glm::vec3{0.0, -1.0, 0.0};
      t.y *= 0.001 * deltaT;
      t.x = ((float)(rand() % 100 - 50) / 10000) * deltaT;
      vVel += t;

      // Write back
      velocities[index] = vVel;
    }
  }

  void update_time(float deltaT) {
    for (auto& time : time_to_live) time -= deltaT;
  }

  void remove_old_particles() {
    auto end = stable_partition_position(particles.begin(), particles.end(),
                                         [this](auto iter) {
                                           auto i = iter - particles.begin();
                                           return time_to_live[i] > 0.0;
                                         });
    particles.erase(end, particles.end());

    auto v_end = stable_partition_position(velocities.begin(), velocities.end(),
                                           [this](auto iter) {
                                             auto i = iter - velocities.begin();
                                             return time_to_live[i] > 0.0;
                                           });
    velocities.erase(v_end, velocities.end());

    {
      auto v_end =
          std::stable_partition(time_to_live.begin(), time_to_live.end(),
                                [](auto el) { return el > 0.0; });
      time_to_live.erase(v_end, time_to_live.end());
    }
  }

  size_t size() { return particles.size(); }

  void init(size_t n) {
    time_to_live.resize(n, 50.0);
    velocities.resize(n);
    particles.resize(n);
    reset();
  }
};
