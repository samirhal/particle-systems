/*
This code is licensed under the Mozilla Public License Version 2.0
(http://opensource.org/licenses/MPL-2.0) � 2014 by Sascha Willems -
http://www.saschawillems.de

This compute shader implements a very basic attraction based particle system
that changes velocities to move the particles towards the target position
*/

#include "glRenderer.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <algorithm>
#include <fstream>
#include <glm/glm.hpp>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct vertex4f {
  GLfloat x, y, z, w;
};

static string readFile(const char* fileName) {
  string fileContent;
  ifstream fileStream(fileName, ios::in);
  if (!fileStream.is_open()) {
    printf("File %s not found\n", fileName);
    return std::string{};
  }
  string line;
  while (!fileStream.eof()) {
    getline(fileStream, line);
    fileContent.append(line + "\n");
  }
  fileStream.close();
  return fileContent;
}

static GLuint loadBMPTexture(const char* fileName) {
  FILE* bmpFile = fopen(fileName, "rb");
  if (!bmpFile) {
    printf("Could not load bitmap from %s\n", fileName);
    return 0;
  }

  unsigned char* bmpHeader = new unsigned char[54];
  if (fread(bmpHeader, 1, 54, bmpFile) != 54) {
    printf("Headersize does not fit BMP!\n");
    return 0;
  }

  unsigned int width = *(int*)&(bmpHeader[0x12]);
  unsigned int height = *(int*)&(bmpHeader[0x16]);
  // unsigned int dataPos = *(int*)&(bmpHeader[0x0A]) != 0 ?
  // *(int*)&(bmpHeader[0x0A]) : 54;
  unsigned int imageSize = *(int*)&(bmpHeader[0x22]) != 0
                               ? *(int*)&(bmpHeader[0x22])
                               : width * height * 3;

  unsigned char* bmpData = new unsigned char[imageSize];
  fread(bmpData, 1, imageSize, bmpFile);
  fclose(bmpFile);

  // Generate OpenGL texture (and generate mipmaps
  GLuint textureID;
  glGenTextures(1, &textureID);
  glBindTexture(GL_TEXTURE_2D, textureID);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_BGR,
               GL_UNSIGNED_BYTE, bmpData);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                  GL_LINEAR_MIPMAP_LINEAR);
  glGenerateMipmap(GL_TEXTURE_2D);
  return textureID;
}

glRenderer::glRenderer() {
  srand(time(NULL));
}

glRenderer::~glRenderer() {}

void glRenderer::printProgramLog(GLuint program) {
  GLint result = GL_FALSE;
  int logLength;

  glGetProgramiv(program, GL_LINK_STATUS, &result);
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
  if (logLength > 0) {
    GLchar* strInfoLog = new GLchar[logLength + 1];
    glGetProgramInfoLog(program, logLength, NULL, strInfoLog);
    printf("programlog: %s\n", strInfoLog);
    delete[] strInfoLog;
  };
}

void glRenderer::printShaderLog(GLuint shader) {
  GLint result = GL_FALSE;
  int logLength;

  glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
  if (logLength > 0) {
    GLchar* strInfoLog = new GLchar[logLength + 1];
    glGetShaderInfoLog(shader, logLength, NULL, strInfoLog);
    printf("shaderlog: %s\n", strInfoLog);
    delete[] strInfoLog;
  };
}

void glRenderer::generateShaders() {
  mainShader = Shader{"data/shader/vertex.shader", "data/shader/fragment.shader"};
}

void glRenderer::resetBuffers() {
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBOPos);
}

void glRenderer::generateBuffers() {
  // No more default VAO with OpenGL 3.3+ core profile context,
  // so in order to make our SSBOs render create and bind a VAO
  // that's never used again
  GLuint VAO;
  glGenVertexArrays(1, &VAO);
  glBindVertexArray(VAO);

  // Position SSBO
  if (glIsBuffer(SSBOPos)) {
    glDeleteBuffers(1, &SSBOPos);
  };
  glGenBuffers(1, &SSBOPos);
  // Bind to SSBO
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBOPos);
  // Generate empty storage for the SSBO
  glBufferData(GL_SHADER_STORAGE_BUFFER,
               particleSystem.particles.size() * sizeof(vertex4f), NULL,
               GL_DYNAMIC_DRAW);
  allocatedSize = particleSystem.size();
  // Bind buffer to target index 0
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, SSBOPos);
}

void glRenderer::generateTextures() {
  particleTex = loadBMPTexture("data/particle.bmp");
}

void glRenderer::runComputeShader() {
  double cursorX, cursorY;
  int windowWidth, windowHeight;
  glfwGetCursorPos(window, &cursorX, &cursorY);
  glfwGetWindowSize(window, &windowWidth, &windowHeight);

  float deltaT = frameDelta * 0.15 * (pause ? 0.0f : 1.0f);

  particleSystem.update(window, deltaT);
  if (particleSystem.size() > allocatedSize) {
    generateBuffers();
  }
  // do update
  glBindBuffer(GL_SHADER_STORAGE_BUFFER, SSBOPos);
  if (particleSystem.size() > 0) {
    struct vertex4f* Positions = (struct vertex4f*)glMapBufferRange(
        GL_SHADER_STORAGE_BUFFER, 0, particleSystem.size() * sizeof(vertex4f),
        GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);
    for (uint32_t index = 0; index < particleSystem.size(); index++) {
      Positions[index] = {particleSystem.particles[index].x,
                          particleSystem.particles[index].y,
                          particleSystem.particles[index].z, 1.0};
    }
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
  }

  // Set memory barrier on per vertex base to make sure we get what was written
  // by the compute shaders
  glMemoryBarrier(GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT);
}

void glRenderer::renderScene() {
  double frameTimeStart = glfwGetTime();

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE);

  // Run compute shader
  runComputeShader();
  // update();

  // Render scene
  mainShader->use();
  // glUseProgram(baseshader);

  glUniform4f(glGetUniformLocation(mainShader->ID, "inColor"),
              particleSystem.color[0] / 255.0f,
              particleSystem.color[1] / 255.0f,
              particleSystem.color[2] / 255.0f, 1.0f);

  glGetError();

  glBindTexture(GL_TEXTURE_2D, particleTex);

  GLuint posAttrib = glGetAttribLocation(mainShader->ID, "pos");

  glBindBuffer(GL_ARRAY_BUFFER, SSBOPos);
  glVertexAttribPointer(posAttrib, 4, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(posAttrib);
  glPointSize(16);
  glDrawArrays(GL_POINTS, 0, particleSystem.size());

  glfwSwapBuffers(window);

  frameDelta = (float)(glfwGetTime() - frameTimeStart) * 100.0f;
}

void glRenderer::keyCallback(int key, __attribute__((unused)) int scancode,
                             int action, __attribute__((unused)) int mods) {
  if (key == GLFW_KEY_R && action == GLFW_PRESS) resetBuffers();
  if (key == GLFW_KEY_C && action == GLFW_PRESS)
    particleSystem.colorFade = !particleSystem.colorFade;
  if (key == GLFW_KEY_P && action == GLFW_PRESS) pause = !pause;
  if (key == GLFW_KEY_PAGE_UP && action == GLFW_PRESS) {
    particleSystem.increase(1024);
    printf("particle count : %ld\nRegenerating SSBOs...\n",
           particleSystem.size());
    generateBuffers();
    particleSystem.reset();
  }
  if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_PRESS &&
      particleSystem.size() > 1024) {
    particleSystem.decrease(1024);
    printf("particle count : %ld\nRegenerating SSBOs...\n",
           particleSystem.size());
    generateBuffers();
    particleSystem.reset();
  }
}
